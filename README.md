# Goldpinger

This repo deploys [Goldpinger](https://github.com/bloomberg/goldpinger).

Goldpinger runs on all nodes and sends http requests from and to each pod.

The connectivity can be verified via a UI.

Prometheus collects metrics and displays them via Grafana.
