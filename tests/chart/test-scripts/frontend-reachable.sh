#!/bin/bash
set -euxo pipefail

curl --max-time 30 --connect-timeout 30 --fail --silent goldpinger | grep '<title>Goldpinger</title>'
